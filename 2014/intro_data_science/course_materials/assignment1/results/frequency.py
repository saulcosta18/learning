"""
Usage: `python frequency.py <tweet_file>`\

(c) Saul Costa
All Rights Reserved
July, 2014
"""

import json
import operator
import re
import sys

class Frequency(object):

  def __init__(self, tweet_file):
    self.tweet_file = open(tweet_file)
    self.frequency = {}
    self.total_terms = 0

  def _process_tweet(self, tweet):
    for word in self.terms(tweet):
      self.total_terms += 1
      if not self.frequency.get(word):
        self.frequency[word] = 1
      else:
        self.frequency[word] += 1


  def run(self):
    for line in self.tweet_file:
      data = json.loads(line)
      if self.delete_tweet(data):
        continue
      tweet = data['text']
      self._process_tweet(tweet)
    sorted_frequency = sorted(
      self.frequency.iteritems(),
      key=operator.itemgetter(1),
      reverse=True
    )
    for (term, frequency) in sorted_frequency:
      sys.stdout.write("%s %.5f\n" % (
        term, (float(frequency / float(self.total_terms)))
      ))


  @staticmethod
  def delete_tweet(data):
    return 'delete' in data.keys()

  @staticmethod
  def terms(tweet):
    return [word.lower() for word in re.findall(r'[a-zA-Z]+', tweet)]

def main():
  freq = Frequency(sys.argv[1])
  freq.run()



if __name__ == '__main__':
  sys.exit(main())