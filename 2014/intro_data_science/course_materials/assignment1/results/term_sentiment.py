"""
Usage: `python term_sentiment.py AFINN-111.txt output.txt > results.txt

(c) Saul Costa
All Rights Reserved
July, 2014
"""

import json
import re
import sys

class TermSentiment(object):

  def __init__(self, sent_file, tweet_file):
    self.sent_file = open(sent_file)
    self.tweet_file = open(tweet_file)
    self.scores = self._gen_scores()
    self.terms_positive = {}
    self.terms_negative = {}

  def _gen_scores(self):
    scores = {}
    for line in self.sent_file:
      term, score  = line.split('\t')
      scores[term] = int(score)
    return scores

  def _compute_term_scores(self, tweet):
    tweet_score = self._sum_scores(tweet)
    missing_words = self._missing_words(tweet)
    for word in missing_words:
      self._process_term(word, tweet_score)

  def _process_term(self, term, score):
    if not self.terms_positive.get(term):
      self.terms_positive[term] = 0
      self.terms_negative[term] = 0
    if score >= 0:
      self.terms_positive[term] += 1
    else:
      self.terms_negative[term] += 1

  def _sum_scores(self, tweet):
    return sum([
      score for score in [
        self.scores.get(word.lower(), 0) for word in re.findall(r'[a-zA-Z]+', tweet)
      ]
      if score != None
    ])

  def _missing_words(self, tweet):
    return [
      word.lower() for word in re.findall(r'[a-zA-Z]+', tweet)
      if str(word.lower()) not in self.scores.keys()
    ]

  def run(self):
    for line in self.tweet_file:
      data = json.loads(line)
      if self.delete_tweet(data):
        continue
      tweet = data['text']
      self._compute_term_scores(tweet)
    for term in self.terms_positive.keys():
      if self.terms_negative[term] == 0:
        sys.stdout.write("%s %.3f\n" % (term, float(self.terms_positive[term])))
      else:
        sys.stdout.write("%s %.3f\n" % (term, (float(self.terms_positive[term]) / float(self.terms_negative[term]))))

  @staticmethod
  def delete_tweet(data):
    return 'delete' in data.keys()


def main():
    term_sentiment = TermSentiment(sys.argv[1], sys.argv[2])
    term_sentiment.run()

if __name__ == '__main__':
    sys.exit(main())
