"""
Usage: `python top_ten.py <tweet_file>`

(c) Saul Costa
All Rights Reserved
July, 2014
"""

import json
import operator
import sys

class TopTen(object):

  def __init__(self, tweet_file):
    self.tweet_file = open(tweet_file)
    self.hashtags = {}

  def _process_hashtags(self, hashtags):
    for hashtag in hashtags:
      hashtag = hashtag['text']
      if not self.hashtags.get(hashtag):
        self.hashtags[hashtag] = 1
      else:
        self.hashtags[hashtag] += 1

  def run(self):
    for line in self.tweet_file:
      data = json.loads(line)
      if self.delete_tweet(data):
        continue
      hashtags = data.get('entities').get('hashtags')
      if not hashtags:
        continue
      self._process_hashtags(hashtags)
    sorted_hashtags = sorted(
      self.hashtags.iteritems(),
      key=operator.itemgetter(1),
      reverse=True
    )
    for (hashtag, count) in sorted_hashtags[:10]:
      sys.stdout.write("%s %d\n" % (hashtag, count))

  @staticmethod
  def delete_tweet(data):
    return 'delete' in data.keys()

def main():
  tt = TopTen(sys.argv[1])
  tt.run()


if __name__ == '__main__':
  sys.exit(main())