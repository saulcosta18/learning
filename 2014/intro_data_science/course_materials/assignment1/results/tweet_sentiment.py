"""
Usage: `python tweet_sentiment.py AFINN-111.txt output.txt > scores.txt`

(c) Saul Costa
All Rights Reserved
July, 2014
"""

import json
import re
import sys


class TweetSentiment(object):

  def __init__(self, sent_file, tweet_file):
    self.sent_file = open(sent_file)
    self.tweet_file = open(tweet_file)
    self.scores = self._gen_scores()

  def _gen_scores(self):
    scores = {}
    for line in self.sent_file:
      term, score  = line.split('\t')
      scores[term] = int(score)
    return scores

  def _sum_scores(self, tweet):
    return sum([
      score for score in [
        self.scores.get(word.lower(), 0) for word in re.findall(r'[a-zA-Z]+', tweet)
      ]
      if score != None
    ])

  def run(self):
    for line in self.tweet_file:
      data = json.loads(line)
      if self.delete_tweet(data):
        continue
      tweet = data['text']
      sys.stdout.write("%d\n" % self._sum_scores(tweet))

  @staticmethod
  def delete_tweet(data):
    return 'delete' in data.keys()


def main():
  ts = TweetSentiment(sys.argv[1], sys.argv[2])
  ts.run()

if __name__ == '__main__':
    sys.exit(main())
