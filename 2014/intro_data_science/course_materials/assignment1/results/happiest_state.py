"""
Usage: `python happiest_state.py AFINN-111.txt output.txt > scores.txt`

(c) Saul Costa
All Rights Reserved
July, 2014
"""

import json
import operator
import re
import sys


VALID_STATES = states = {
        'AK': 'Alaska',
        'AL': 'Alabama',
        'AR': 'Arkansas',
        'AS': 'American Samoa',
        'AZ': 'Arizona',
        'CA': 'California',
        'CO': 'Colorado',
        'CT': 'Connecticut',
        'DC': 'District of Columbia',
        'DE': 'Delaware',
        'FL': 'Florida',
        'GA': 'Georgia',
        'GU': 'Guam',
        'HI': 'Hawaii',
        'IA': 'Iowa',
        'ID': 'Idaho',
        'IL': 'Illinois',
        'IN': 'Indiana',
        'KS': 'Kansas',
        'KY': 'Kentucky',
        'LA': 'Louisiana',
        'MA': 'Massachusetts',
        'MD': 'Maryland',
        'ME': 'Maine',
        'MI': 'Michigan',
        'MN': 'Minnesota',
        'MO': 'Missouri',
        'MP': 'Northern Mariana Islands',
        'MS': 'Mississippi',
        'MT': 'Montana',
        'NA': 'National',
        'NC': 'North Carolina',
        'ND': 'North Dakota',
        'NE': 'Nebraska',
        'NH': 'New Hampshire',
        'NJ': 'New Jersey',
        'NM': 'New Mexico',
        'NV': 'Nevada',
        'NY': 'New York',
        'OH': 'Ohio',
        'OK': 'Oklahoma',
        'OR': 'Oregon',
        'PA': 'Pennsylvania',
        'PR': 'Puerto Rico',
        'RI': 'Rhode Island',
        'SC': 'South Carolina',
        'SD': 'South Dakota',
        'TN': 'Tennessee',
        'TX': 'Texas',
        'UT': 'Utah',
        'VA': 'Virginia',
        'VI': 'Virgin Islands',
        'VT': 'Vermont',
        'WA': 'Washington',
        'WI': 'Wisconsin',
        'WV': 'West Virginia',
        'WY': 'Wyoming'
}

class HappiestState(object):

  def __init__(self, sent_file, tweet_file):
    self.sent_file = open(sent_file)
    self.tweet_file = open(tweet_file)
    self.scores = self._gen_scores()
    self.states = {}

  def _gen_scores(self):
    scores = {}
    for line in self.sent_file:
      term, score  = line.split('\t')
      scores[term] = int(score)
    return scores

  def _sum_scores(self, tweet):
    return sum([
      score for score in [
        self.scores.get(word.lower(), 0) for word in re.findall(r'[a-zA-Z]+', tweet)
      ]
      if score != None
    ])

  def _process_tweet(self, tweet, state):
    tweet_score = self._sum_scores(tweet)
    if not self.states.get(state):
      self.states[state] = tweet_score
    else:
      self.states[state] += tweet_score


  def _try_location(self, data):
    place = data.get('place')
    if place:
      city = place.get('full_name')
      if city:
        state_abv = city.replace(" ", "").split(",")[-1]
        if state_abv in VALID_STATES.keys():
          return state_abv
    return None

  def run(self):
    for line in self.tweet_file:
      data = json.loads(line)
      if self.delete_tweet(data):
        continue
      location = self._try_location(data)
      if not location:
        continue
      self._process_tweet(data['text'], location)
    sorted_states = sorted(
      self.states.iteritems(),
      key=operator.itemgetter(1),
      reverse=True
    )
    for (state, score) in sorted_states:
      sys.stdout.write("%s\n" % state)
      break

  @staticmethod
  def delete_tweet(data):
    return 'delete' in data.keys()


def main():
  hs = HappiestState(sys.argv[1], sys.argv[2])
  hs.run()

if __name__ == '__main__':
    sys.exit(main())
